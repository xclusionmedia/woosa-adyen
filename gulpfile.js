const gulp = require('gulp');
const path = require('path');
const cleanCss = require('gulp-clean-css');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const uglify = require('gulp-uglify');
const webpack = require('webpack-stream');
const named = require('vinyl-named');
const del = require('del');
const exec = require('child_process').exec;

const currentFolderPath = path.resolve(__dirname, './');
const currentFolderName = path.basename(path.dirname(currentFolderPath+'/gulpfile.js'));



/**
 * Development
 */

//dev vendor
gulp.task('dev-vendor', function (cb) {
   exec('composer install', function (err, stdout, stderr) {
      console.log(stdout);
      console.log(stderr);
      cb(err);
   });
});

//transpile ES6
gulp.task('es6-to-js', function () {
   return gulp.src(['assets/es6/*.js', '!assets/es6/_*.js', '!assets/es6/*.min.js'])
      .pipe(named())
      .pipe(webpack({
         mode: 'development'
      }))
      .pipe(gulp.dest('assets/js'));
});

//convert SCSS
gulp.task('scss-to-css', function () {
   return gulp.src(['assets/scss/*.scss', '!assets/scss/_*.scss'])
      .pipe(sass())
      .pipe(gulp.dest('assets/css'));
});

//delete dist folder
gulp.task('clean-dist', function () {
   return del([
      'dist'
   ]);
});

//copy JS files
gulp.task('copy-files-to-js', function () {
   return gulp.src(['assets/es6/*.min.js'])
      .pipe(gulp.dest('assets/js'));
});

//copy CSS files
gulp.task('copy-files-to-css', function () {
   return gulp.src(['assets/scss/*.min.css'])
      .pipe(gulp.dest('assets/css'));
});


/**
 * Distribution
 */

//pack JS
gulp.task('pack-dist-js', function () {
   return gulp.src([
      'assets/js/*.js',
      '!assets/js/*.min.js',
      '!assets/js/*-min.js',
      '!assets/js/*googlepay.js'
   ])
   .pipe(named())
   .pipe(webpack({
      mode: 'production'
   }))
   .pipe(uglify())
   .pipe(gulp.dest('dist/'+currentFolderName+'/assets/js'));
});

//pack CSS
gulp.task('pack-dist-css', function () {
   return gulp.src([
      'assets/css/*.css',
      '!assets/css/*.min.css',
      '!assets/css/*-min.css'
   ])
   .pipe(cleanCss())
   .pipe(gulp.dest('dist/'+currentFolderName+'/assets/css'));
});

//prod vendor
gulp.task('prod-vendor', function (cb) {
   exec('composer install --no-dev', function (err, stdout, stderr) {
      console.log(stdout);
      console.log(stderr);
      cb(err);
   });
});

//build plugin folder
gulp.task('pack-dist-files', function () {
   return gulp.src([
      'assets/**/*',
      '!assets/js/**',
      '!assets/es6/**',
      '!assets/css/**',
      '!assets/scss/**',
      'includes/**/*',
      'languages/**/*',
      'vendor/**/*',
      'templates/**/*',
      'readme.txt',
      '*.php',
   ], {
      base: currentFolderPath
   })
   .pipe(gulp.dest('dist/'+currentFolderName));
});
gulp.task('pack-min-files', function () {
   return gulp.src([
      'assets/js/*.min.js',
      'assets/js/*.-min.js',
      'assets/js/*googlepay.js',
      'assets/css/*.min.css',
      'assets/css/*.-min.css',
   ], {
      base: currentFolderPath
   })
   .pipe(gulp.dest('dist/'+currentFolderName));
});


//--------
gulp.task('watch', function() {
   gulp.watch('assets/es6/*.js', gulp.series('es6-to-js', 'copy-files-to-js'));
   gulp.watch('assets/scss/*.scss', gulp.series('scss-to-css', 'copy-files-to-css'));
});

gulp.task('default', gulp.series('dev-vendor', 'clean-dist', 'es6-to-js', 'scss-to-css', 'copy-files-to-css', 'copy-files-to-js', 'watch'));

gulp.task('build', gulp.series('prod-vendor', 'clean-dist', 'es6-to-js', 'scss-to-css', 'pack-dist-js', 'pack-dist-css', 'pack-dist-files', 'pack-min-files'));