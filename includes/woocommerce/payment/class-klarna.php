<?php
/**
 * Klarna
 *
 * This class creates payment method: Klarna - Pay later
 *
 * @package Woosa-Adyen/WooCommerce/Payment
 * @author Woosa Team
 * @since 1.1.0
 */

namespace Woosa\Adyen;


//prevent direct access data leaks
defined( 'ABSPATH' ) || exit;


class Klarna extends Ideal{


   /**
    * Constructor of this class.
    *
    * @since 1.1.0
    */
   public function __construct(){

      parent::__construct();

      $this->has_fields = false;

      if( 'yes' !== get_option(PREFIX .'_auto_klarna_payments') ){

         $this->supports = [
            'products',
            'refunds',
         ];

      }

   }



   /**
    * List of countries where is available.
    *
    * @since 1.1.0
    * @return array
    */
   public function available_countries(){

      return [
         'AT' => [
            'currencies' => ['EUR'],
            'recurring' => true,
         ],
         'BE' => [
            'currencies' => ['EUR'],
            'recurring' => false,//klarna refuse to process (not in Klarna doc for recurring)
         ],
         'DK' => [
            'currencies' => ['DKK'],
            'recurring' => true,
         ],
         'FI' => [
            'currencies' => ['EUR'],
            'recurring' => true,
         ],
         'DE' => [
            'currencies' => ['EUR'],
            'recurring' => true,
         ],
         'NO' => [
            'currencies' => ['NOK'],
            'recurring' => true,
         ],
         'SE' => [
            'currencies' => ['SEK'],
            'recurring' => true,//recurring_contract notification not comming
         ],
         'CH' => [
            'currencies' => ['CHF'],
            'recurring' => true,//klarna refuse to process (also for standard)
         ],
         'NL' => [
            'currencies' => ['EUR'],
            'recurring' => true,//klarna refuse to process (also for standard)
         ],
         'GB' => [
            'currencies' => ['GBP'],
            'recurring' => false,//klarna refuse to process (not in Klarna doc for recurring)
         ],
      ];
   }



   /**
    * Gets default payment method title.
    *
    * @since 1.1.0
    * @return string
    */
   public function get_default_title(){
      return __('Adyen - Klarna - Pay later', 'woosa-adyen');
   }



   /**
    * Gets default payment method description.
    *
    * @since 1.2.0 - show supported countries for recurring payments.
    * @since 1.1.0
    * @return string
    */
   public function get_default_description(){

      $output = sprintf(__('Pay after the goods have been delivered. %s', 'woosa-adyen'), '<br/>'.$this->show_supported_country());
      $output .= '<br/>'.$this->show_rec_supported_country();

      return $output;
   }



   /**
    * Gets default description set in settings.
    *
    * @since 1.1.0
    * @return string
    */
   public function get_settings_description(){}



   /**
    * Type of the payment method (e.g ideal, scheme. bcmc).
    *
    * @since 1.1.0
    * @return string
    */
   public function payment_method_type(){
      return 'klarna';
   }



   /**
    * Returns the payment method to be used for recurring payments
    *
    * @since 1.1.0
    * @return string
    */
   public function recurring_payment_method(){
      return $this->payment_method_type();
   }



   /**
    * Validates extra added fields.
    *
    * @since 1.1.0
    * @return bool
    */
   public function validate_fields() {

      $is_valid = Abstract_Gateway::validate_fields();

      return $is_valid;
   }



   /**
    * Builds the required payment payload
    *
    * @since 1.1.0
    * @param \WC_Order $order
    * @param string $reference
    * @return array
    */
   protected function build_payment_payload(\WC_Order $order, $reference){

      $payload = Abstract_Gateway::build_payment_payload($order, $reference);

      return $payload;
   }



   /**
    * Checks whether or not the recurring payments are supported by the country.
    *
    * @since 1.2.0
    * @return bool
    */
   public function support_recurring(){

      if( WC()->cart && $this->get_order_total() > 0 ) {

         if( ! empty($this->available_countries()) ){

            $customer_country = WC()->customer->get_billing_country();
            $country = Utility::rgar($this->available_countries(), $customer_country);

            return Utility::rgar($country, 'recurring') === true ? true : false;
         }
      }

      return true;
   }



   /**
    * Display the countries which support recurring payments.
    *
    * @since 1.2.0
    * @return array
    */
   public function show_rec_supported_country(){

      $output = '';
      $countries = [];

      foreach($this->available_countries() as $country_code => $data){

         $country_code = '_ANY_' === $country_code ? __('ANY', 'woosa-adyen') : $country_code;

         if(Utility::rgar($data, 'recurring') === true){
            if( empty(Utility::rgar($data, 'currencies')) ){
               $countries[] = $country_code;
            }else{
               $currencies = Utility::rgar($data, 'currencies');
               $countries[] = $country_code . ' ('.implode(', ', $currencies).')';
            }
         }
      }

      if( ! empty($countries) ){
         $output = sprintf(__('%sSupported country for recurring payments:%s %s', 'woosa-adyen'), '<b>', '</b>', implode(', ', $countries));
      }

      return $output;
   }


}