import { ADYEN, prefixIt, elemSendAjaxCall } from "./_functions";


/**
 * Sends AJAX calls triggered by a given element
 *
 * @since 1.1.3
 */
elemSendAjaxCall( '[data-'+prefixIt('-action')+']' );


jQuery(function($){

   $(document).on('click', '[type="submit"]', function(){
      window.onbeforeunload = null;//prevent the window which says "the changes may be lost"
   });

   //API test mode
   $( prefixIt('_testmode', '#') ).change( function() {
      if ( $( this ).is(':checked') ) {
         $( this ).closest('tbody').find( '.api_testmode_field' ).closest( 'tr' ).show();
      } else {
         $( this ).closest('tbody').find( '.api_testmode_field' ).closest( 'tr' ).hide();
      }
   }).change();



   /**
    * Captures payment
    * *@since 1.1.0 - add additional confirmation window
    * *@since 1.0.3
    */
   $('[data-capture-order-payment]').on('click', function(){

      let _this = $(this),
         order_id = _this.attr('data-capture-order-payment');

      if(confirm(ADYEN().translation.perform_action)){

         $.ajax({
            url: ADYEN().ajax.url,
            method: 'POST',
            data: {
               action: prefixIt('_capture_payment'),
               security: ADYEN().ajax.nonce,
               order_id: order_id
            },
            beforeSend: function(){
               _this.data('label', _this.text()).text(ADYEN().translation.processing).prop('disabled', true);
            },
            success: function(res) {

               if(res.success){
                  window.location.reload();
               }else{
                  _this.text(_this.data('label')).prop('disabled', false);
               }
            }
         });
      }

   });

});