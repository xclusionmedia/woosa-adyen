import { prefixIt, ADYEN, blockSection, unBlockSection } from "./_functions";


jQuery(function($){


   /**
    * Init notification box
    * @since 1.0.0
    */
   setTimeout(function(){
      $(prefixIt('-log-notification', '.')).each(function(index, item){
         let offset = 110,
            bottom = index == 0 ? 10 : offset * index;

         $(this).animate({
            'bottom': bottom
         }).slideDown('fast');
      });
   }, 800);


   /**
    * Close notification box
    * @since 1.0.0
    */
   $(document).on('click', prefixIt('-log-notification__close', '.'), function(){
      $(this).closest(prefixIt('-log-notification', '.')).hide();

      //refresh bottom position
      $(prefixIt('-log-notification:visible', '.')).each(function(index, item){
         let offset = 110,
            bottom = index == 0 ? 10 : offset * index;

         $(this).animate({
            'bottom': bottom
         }).slideDown('fast');
      });
   });


   /**
    * Enable the action buttons when at least a checkbox field is checked
    * @since 1.0.0
    */
   $(document).on('change', '[data-'+prefixIt('-log-checkbox')+']', function(){

      window.onbeforeunload = null;//prevent the window which says "the changes may be lost"

      let _this = $(this),
         log_list = _this.closest(prefixIt('-logs', '.'));

      if(log_list.find('input:checkbox:checked').length > 0){
         log_list.find(prefixIt('-log-actions__right button', '.')).prop('disabled', false);
      }else{
         log_list.find(prefixIt('-log-actions__right button', '.')).prop('disabled', true);
      }

   });



   /**
    * Runs the selected action.
    * @since 1.0.0
    */
   $(document).on('click', '[data-'+prefixIt('-log-action')+']', function(e){

      let _this = $(this),
         log_action = _this.attr('data-'+prefixIt('-log-action')),
         log_elem = _this.closest('[data-'+prefixIt('-log-code')+']'),
         log_list = _this.closest(prefixIt('-logs', '.')),
         log_code = log_elem.attr('data-'+prefixIt('-log-code'));

      if('view_detail' === log_action){

         let url = ADYEN().ajax.url+'?action='+prefixIt('_log_action')+'&security='+ADYEN().ajax.nonce+'&log_action='+log_action+'&log_code='+log_code+'&width=700&height=700';

         tb_show('Log detail', url);

      }else if('select_all' === log_action){

         let checked = _this.data('checked');

         if(checked == true){

            _this.data('checked', false);
            checked = false;
            _this.removeClass('button-primary');

            log_list.find(prefixIt('-log-actions__right button', '.')).prop('disabled', true);

         }else{

            _this.data('checked', true);
            checked = true;
            _this.addClass('button-primary');

            log_list.find(prefixIt('-log-actions__right button', '.')).prop('disabled', false);
         }

         log_list.find('input:checkbox').prop('checked', checked);

      }else{

         let log_codes = [];

         log_list.find('input:checkbox:checked').each(function(){
            log_codes.push($(this).val());
         });

         if('remove' === log_action){
            if( ! window.confirm('Are you sure you want to remove this log?') ) {
               return;
            }
         }

         $.ajax({
            url: ADYEN().ajax.url,
            method: 'POST',
            data: {
               action: prefixIt('_log_action'),
               security: ADYEN().ajax.nonce,
               log_action: log_action,
               log_codes: log_codes,
            },
            beforeSend: function(){

               _this.attr('disabled', true);
               blockSection('#wpcontent');

            },
            success: function(res){

               if(res.success){

                  window.location.reload();

               }else{

                  console.error(res.data.message);

                  _this.attr('disabled', false);
                  unBlockSection('#wpcontent');
               }
            },
         });
      }

   });

});