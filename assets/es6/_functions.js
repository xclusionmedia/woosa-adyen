export function add(x, y){
   return x + y;
}

export function ADYEN(){
   return woosa_adn;
}

export function prefixIt(selector, type = ''){
   return type+ADYEN().prefix+selector;
}

export function blockSection(elem) {
   jQuery(elem).block({
      message: null,
      overlayCSS: {
         background: '#fff',
         opacity: 0.6
      }
   });
}

export function unBlockSection(elem) {
   jQuery(elem).unblock();
}

export function isJson(str) {
   try{
      JSON.parse(str);
   }catch (e){
      //Error
      //JSON is not okay
      return false;
   }

  return true;
}

export function elemSendAjaxCall(target){

   jQuery(function($){

      let is_processing = false;

      $(document).on('click', target, function(e){

         e.preventDefault();

         let btn = $(this),
            action = btn.attr('data-'+prefixIt('-action')),
            extra = isJson(btn.attr('data-'+prefixIt('-extra'))) ? JSON.parse(btn.attr('data-'+prefixIt('-extra'))) : btn.attr('data-'+prefixIt('-extra')),
            processing_label = ADYEN().translation.processing,
            parentElem = btn.parent(),
            fields = btn.closest('#mainform').find(':input').serialize();

         if(is_processing) return;

         $.ajax({
            url: ADYEN().ajax.url,
            method: 'POST',
            data: {
               action: prefixIt('_'+action),
               security: ADYEN().ajax.nonce,
               extra: extra,
               fields: fields,
            },
            beforeSend: function(){

               is_processing = true;

               blockSection('#wpcontent');

               btn.data('label', btn.text());
               btn.text(processing_label).attr('disabled', true);

               parentElem.parent().find(prefixIt('-error-text', '.')).remove();
            },
            success: function(res){

               if(res.success){
                  window.location.reload();
               }else{
                  $('<p class="'+prefixIt('-error-text')+'">'+res.data.message+'</p>').insertAfter(parentElem);
                  btn.text( btn.data('label') ).attr('disabled', false);
                  unBlockSection('#wpcontent');
               }
            },
            complete: function(){

               is_processing = false;
            }
         });
      });

   });

}

export function debugLog(msg){
   if(ADYEN().debug){
      console.log('%cDEBUG LOG: '+msg, "color: orange");
   }
}



/**
 * Clear card form data from hidden fileds.
 *
 * @since 1.1.1 - change function name
 * @since 1.0.6
 * @param {string} methodType
 */
export function clearCardForm(methodType){

   jQuery(prefixIt('-'+methodType+'-card-number', '#')).val('');
   jQuery(prefixIt('-'+methodType+'-card-exp-month', '#')).val('');
   jQuery(prefixIt('-'+methodType+'-card-exp-year', '#')).val('');
   jQuery(prefixIt('-'+methodType+'-card-cvc', '#')).val('');
   jQuery(prefixIt('-'+methodType+'-card-holder', '#')).val('');
   jQuery(prefixIt('-'+methodType+'-sci', '#')).val('');
   jQuery(prefixIt('-'+methodType+'-store-card', '#')).val('');

   debugLog('Clear the encrypted card data.');

}


/**
 * Fills the encrypted card data in hidden fields.
 *
 * @since 1.1.1
 * @param {object} state
 */
export function setEncryptedCardData(state, component = {}, methodType = ''){

   if(state && state.isValid){
      let store_card = state.data.storePaymentMethod ? state.data.storePaymentMethod : '0';

      if(state.data.paymentMethod.type == 'scheme'){

         if(component._node){

            jQuery('#'+component._node.id).data('card_state', state);

            debugLog('Saved temporarily the encrypted card data on the element.');

         }

         jQuery(prefixIt('-'+methodType+'-card-number', '#')).val(state.data.paymentMethod.encryptedCardNumber);
         jQuery(prefixIt('-'+methodType+'-card-exp-month', '#')).val(state.data.paymentMethod.encryptedExpiryMonth);
         jQuery(prefixIt('-'+methodType+'-card-exp-year', '#')).val(state.data.paymentMethod.encryptedExpiryYear);
         jQuery(prefixIt('-'+methodType+'-card-cvc', '#')).val(state.data.paymentMethod.encryptedSecurityCode);
         jQuery(prefixIt('-'+methodType+'-card-holder', '#')).val(state.data.paymentMethod.holderName);
         jQuery(prefixIt('-'+methodType+'-sci', '#')).val(state.data.paymentMethod.storedPaymentMethodId);
         jQuery(prefixIt('-'+methodType+'-store-card', '#')).val(store_card);
         jQuery(prefixIt('-'+methodType+'-card-installments', '#')).val(state.data.installments.value);

         debugLog('Set the encrypted card data.');

      }

   }else{

      clearCardForm(methodType);
   }
}


/**
 * Generates the card form fields.
 *
 * @since 1.2.0 - set/update installments value
 * @since 1.1.3 - add support for Web Component v4.4.0 - configuration object should be pased to create()
 * @since 1.1.1 - do not mount the component everytime a click takes place but only set the encrypted data
 *              - change function name
 * @since 1.0.0
 * @param {object} checkout
 */
export function generateCardForm(checkout){

   jQuery('[data-'+prefixIt('-stored-card')+']').off('click').on('click', function(){

      let _this        = jQuery(this),
         parent        = _this.parent(),
         current       = parent.find(prefixIt('-stored-card__fields', '.')),
         methodType    = _this.attr('data-'+prefixIt('-stored-card-type')),
         formElemId    = _this.attr('data-'+prefixIt('-stored-card')),
         formElem      = jQuery('#'+formElemId),
         formType      = 'scheme' === methodType ? 'card' : methodType,
         cardState     = formElem.data('card_state'),
         methodIndex   = formElemId.replace(/[^0-9\.]/g, ''),
         storedMethods = checkout.paymentMethodsResponse.storedPaymentMethods,
         card_installments = jQuery('[data-'+prefixIt('-card-installments')+']').val(),
         paymentMethodsConfiguration = '';


      if(formElem.length > 0){

         //new card
         if( '' === methodIndex){

            if( '' == formElem.children(0).html() ){

               if(isJson(card_installments)){

                  card_installments = JSON.parse(card_installments);

                  if(card_installments.constructor === Array){

                     paymentMethodsConfiguration = {
                        card: {
                           installmentOptions: {
                              card: {
                                 values: card_installments,
                                 // Shows regular and revolving as plans shoppers can choose.
                                 // plans: [ 'regular', 'revolving' ]
                              },
                           },
                           // Shows payment amount per installment.
                           showInstallmentAmounts: true
                        }
                     }
                  }
               }

               checkout.setOptions({
                  paymentMethodsConfiguration: paymentMethodsConfiguration,
               });

               checkout.create(formType, {
                  brands: ADYEN().api.card_types,
                  enableStoreDetails: ADYEN().api.store_card,
                  hasHolderName: true,
                  holderNameRequired: true,
               }).mount('#'+formElemId);

               debugLog('Initiated the form for using a new card.');

            }else{

               setEncryptedCardData(cardState, {}, methodType);
            }

            jQuery(prefixIt('-'+methodType+'-is-stored-card', '#')).val('no');

         //stored card
         }else{

            if('' == formElem.html()){

               checkout.create(formType, storedMethods[methodIndex]).mount('#'+formElemId);

               debugLog('Initiated the form for the existing card.');

            }else{

               setEncryptedCardData(cardState, {}, methodType);
            }

            jQuery(prefixIt('-'+methodType+'-is-stored-card', '#')).val('yes');
         }
      }

      jQuery(prefixIt('-stored-card__fields', '.')).closest(prefixIt('-stored-card', '.')).not(parent).removeClass('selected');
      jQuery(prefixIt('-stored-card__fields', '.')).not(current).slideUp();

      current.slideToggle();
      parent.addClass('selected');
   });
}


/**
 * Generates AppleyPay button.
 * @since unkown
 * @param {object} checkout
 */
export function generateApplePay(checkout){

   // const apple_merchant_name = jQuery('#woosa_adyen_applepay_merchant_name').val(),
   //       apple_merchant_identifier = jQuery('#woosa_adyen_applepay_merchant_identifier').val();

   // const applepay = checkout.create("applepay", {
   //    currencyCode: ADYEN().currency,
   //    amount: (ADYEN().cart.total) * 100, //it's in cents
   //    countryCode: ADYEN().cart.country,
   //    configuration: {
   //       merchantName: apple_merchant_name,
   //       merchantIdentifier: apple_merchant_identifier
   //    },
   //    // Button config
   //    buttonType: "buy", // Optional. The type of button you want to be displayed in the payments form
   //    buttonColor: "black", // Optional. Specify the color of the button
   //    onValidateMerchant: (resolve, reject, validationURL) => {
   //       // Call the validation endpoint with validationURL.
   //       // Call resolve(MERCHANTSESSION) or reject() to complete merchant validation.
   //    }
   // });

   // applepay
   //    .isAvailable()
   //    .then(() => {
   //       applepay.mount("#applepay-container");
   //    })
   //    .catch(e => {
   //       console.log(e)
   //       jQuery('.payment_method_woosa_adyen_applepay').remove();
   //    });
}



/**
 * Generates GooglePay button.
 *
 * @since 1.1.0
 * @param {object} checkout
 */
export function generateGooglePay(checkout){

   if(jQuery('#woosa_adyen_googlepay_button').length > 0){

      const test_mode = 'yes' !== jQuery('#woosa_adyen_googlepay_testmode').val() && 'yes' !== ADYEN().api.test_mode ? false : true,
         merchant_id = jQuery('#woosa_adyen_googlepay_merchant_identifier').val();

      let config = {
         gatewayMerchantId: ADYEN().api.adyen_merchant,
         merchantName: ADYEN().site_name,
         merchantId: merchant_id
      };

      const googlepay = checkout.create("paywithgoogle", {
         environment: test_mode ? 'TEST' : 'PRODUCTION',
         amount: {
            currency: ADYEN().currency,
            value: (ADYEN().cart.total) * 100, //it's in cents
         },
         configuration: config,
         buttonColor: "white",
         onAuthorized: (data) => {

            jQuery('#'+ADYEN().prefix+'-googlepay-container .googlepay-description').html(data.paymentMethodData.description).show();
            jQuery('#woosa_adyen_googlepay_description').val(data.paymentMethodData.description);
            jQuery('#woosa_adyen_googlepay_token').val(data.paymentMethodData.tokenizationData.token);
         }
      });


      googlepay
         .isAvailable()
         .then(() => {

            if(jQuery('.adyen-checkout__paywithgoogle').length == 0){
               googlepay.mount("#woosa_adyen_googlepay_button");
            }

         })
         .catch(e => {
            console.log(e);
            jQuery('.wc_payment_method .payment_method_woosa_adyen_googlepay').remove();
         });
   }
}
