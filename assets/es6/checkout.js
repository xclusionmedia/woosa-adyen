import { ADYEN, prefixIt, setEncryptedCardData, generateCardForm, generateGooglePay, generateApplePay } from "./_functions";

var popup = require("jquery-popup-overlay");

jQuery(function($){

   if(typeof AdyenCheckout == 'undefined') return;


   /**
    * Init popup used for payment actions
    * @since 1.0.0
    */
   $(prefixIt('-popup', '.')).each(function(){
      let _this = $(this),
         blur = (typeof _this.attr('data-blur') == 'undefined') ? false : true,
         escape = (typeof _this.attr('data-escape') == 'undefined') ? false : true;

      _this.popup({
         autoopen: true,
         blur: blur,
         escape: escape,
         scrolllock: true
      });
   });

   let payment_actions = [
      prefixIt('-card-action', '#'),
      prefixIt('-boleto-action', '#'),
      prefixIt('-alipay-action', '#'),
      prefixIt('-wechatpay-action', '#'),
      prefixIt('-googlepay-action', '#'),
   ];



   /**
    * Init Adyen's Card component
    * @since 1.1.3 - updates according to Web component v4.4.0
    * @since 1.1.1 - change `originKey` to `clientKey`
    * @since 1.0.0
    */
   const checkout = new AdyenCheckout({
      paymentMethodsResponse: ADYEN().api.response_payment_methods,
      clientKey: ADYEN().api.origin_key,
      locale: ADYEN().locale,
      environment: ADYEN().api.test_mode === 'yes' ? 'test' : 'live',
      onChange: function (state, component) {

         let methodType = 'bcmc' === component.props.type ? 'bcmc' : state.data.paymentMethod.type;

         setEncryptedCardData(state, component, methodType);

      },
      onAdditionalDetails: function (state, component) {

         let elem = $(component._node),
            order_id = elem.attr('data-order_id'),
            action_key = component.props.dataKey;//threeds2.fingerprint, threeds2.challengeResult

         // send for processing
         $.ajax({
            url: ADYEN().ajax.url,
            method: 'POST',
            data: {
               action: prefixIt('_send_payment_details'),
               security: ADYEN().ajax.nonce,
               action_data: state.data,
               order_id: order_id
            },
            success: function(res) {
               if(res.data.redirect){
                  window.location.href = res.data.redirect;
               }
            }
         });

      },
      onError: function(err){
         if(ADYEN().debug){
            console.log(err)
         }
      }
   });



   /**
    * Run after checkout is updated
    *
    * @since 1.1.1 - remove Giropay web component
    * @since 1.0.3 - add support for saving credit cards
    * @since 1.0.0
    */
   $(document).on('updated_checkout', function(){

      if($(prefixIt('-datepicker', '.')).length > 0){
         $(prefixIt('-datepicker', '.')).datepicker({
            dateFormat : "dd-mm-yy",
            changeYear: true,
            changeMonth: true,
         })
      }

      generateCardForm(checkout);
      generateGooglePay(checkout);
      generateApplePay(checkout)

   });
   generateCardForm(checkout);
   generateGooglePay(checkout);
   generateApplePay(checkout)



   /**
    * Create payment action based on the received action data
    * @since 1.0.4
    */
   payment_actions.map(function(item){

      if($(item).length > 0){

         let action = JSON.parse($(item).attr('data-payment_action'));

         checkout.createFromAction(action).mount(item);
      }

   });



});
