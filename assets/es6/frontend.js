import { ADYEN, prefixIt } from "./_functions";

jQuery(function($){

   /**
    * Remove a given card
    * @since 1.0.3
    */
   $('[data-remove-sci]').on('click', function(){

      let _this = $(this),
         reference = _this.attr('data-remove-sci');

      if(confirm(ADYEN().translation.remove_card)){

         $.ajax({
            url: ADYEN().ajax.url,
            method: 'POST',
            data: {
               action: prefixIt('_remove_card'),
               security: ADYEN().ajax.nonce,
               reference: reference
            },
            beforeSend: function(){
               _this.closest(prefixIt('-list-cards__item', '.')).fadeOut();
            },
         });
      }

   });



   /**
    * Sends request to remove data protection
    * @since 1.1.0
    */
   $('[data-remove-gdpr]').on('click', function(){

      let _this = $(this),
         order_id = _this.data('remove-gdpr');

      if(confirm(ADYEN().translation.remove_gdpr)){

         $.ajax({
            url: ADYEN().ajax.url,
            method: 'POST',
            data: {
               action: prefixIt('_remove_gdpr'),
               security: ADYEN().ajax.nonce,
               order_id: order_id
            },
            beforeSend: function(){
               _this
                  .data('label', _this.text())
                  .text(ADYEN().translation.processing)
                  .attr('disabled', true);

               _this
                  .closest('div')
                  .find(prefixIt('-error-text', '.'))
                  .remove();
            },
            success: function(res){

               if(res.success){
                  window.location.reload();
               }else{
                  $('<p class="'+prefixIt('-error-text')+'">'+res.data.message+'</p>').insertAfter(_this.parent());
               }

            },
            complete: function(){
               _this
                  .text(_this.data('label'))
                  .attr('disabled', false);
            }
         });
      }

   });

});