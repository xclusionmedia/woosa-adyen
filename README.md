## Requirements

* `nodejs`
* `composer`
* `PHP 7.1` at least

## Setup

1. run `npm install` to intall JS libraries

1. run `composer install` to install PHP dependencies

1. run `npm start` for development (compiling SCSS to CSS, ES6 to JS)

1. run `npm test` for running JS and PHP tests

1. run `npm run build` to build the files for production
   * `/dist/` folder with be created with the needed files


## Important Notes

* all `.php` files inside of `/includes/` are loaded via `vendor/auload.php` so please make sure you run `composer update` after you create new files that you want to load

* use `Object-oriented programming (OOP)` while develop the functionality

* use namespace to encapsulate your PHP code

* use comments to describe your code

* think modularly, extendable, and optimized as possible

* use SCSS instead of pure CSS

* write ES6 instead of normal JS